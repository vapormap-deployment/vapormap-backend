# PROD Commands

build-prod-be:
	docker build --no-cache -t vapormap-prod-be -f _prod-dep/prod-be.Dockerfile .

build-prod-migrate:
	docker build -t vapormap-migrate -f prod-dep/prod-migrate.Dockerfile .


deploy-prod: build-prod-images
	docker-compose  -p vapormap-prod -f prod-dep/prod.docker-compose.yml up

migrate: build-prod-migrate
	docker run --network="host" vapormap-migrate 

helm-install:
	microk8s helm upgrade --install vapormap-backend _chart
helm-uninstall:
	microk8s helm uninstall vapormap-backend