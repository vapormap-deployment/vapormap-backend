# Guide d'installation de Vapormap

|MAINTAINER   | GIT|
|------------ |--------------|
| Ali HAMOUDA |  https://gitlab.com/vapormap-deployment|


# Introduction
Ce groupe de projet permet d'installer l'application **Vapormap** sur une instance EC2 (AWS).
Le groupe se divise en 3 projets Git: *vapormap-frontend*, *vapormap-backend* et *maintaining-vapormap*.
Dans ce guide d'installation, vous trouverez l'utilité de chaque repo et les instructions d'installation.

# Stack de technologies
Le projet repose sur Django/python pour le backend, web native pour l'application frontend déployé sur un serveur Nginx.
La base de données utilisées est MariaDB.
Nous utilisons *GitLab*  CI pour la construction du pipeline, Docker pour la *contenairisation* ainsi que *Kubernetes* pour l'orchestration. 
Et enfin, pour automatiser l'allocation et la configuration de l'instance sur AWS, nous utilisons Terraform et Ansible.

# Arbre des projets
## Backend
```bash
*__
   |_  _chart                           // Helm Chart du backend
   |_  _prod-dep                        // Docker files du projet
   |_  src                              // Code source
   |_  .gitlab-ci.yml                   // Pipeline
   |_  deploy.sh                        // script de déploiement appelé par le pipeline
   |_  Makefile                         // Makefile pour les commandes
   |_  Readme.md                        // Instructions du projet
```

## Frontend
```bash
*__
   |_  _chart                           // Helm Chart du frontend
   |_  src                              // Code source
   |_  Dockerfile                       // Docker file du projet
   |_  .gitlab-ci.yml                   // Pipeline
   |_  deploy.sh                        // script de déploiement appelé par le pipeline
   |_  Makefile                         // Makefile pour les commandes
   |_  Readme.md                        // Instructions du projet
```

## Maintaining Vapormap
```bash
*__
   |_  IaC                              // Dossier de l\'infra as code en Terraform
   |_  playbook                         // Playbook Ansbile pour configurer l\'instance allouée.
   |_  templates                        // Contient des templates pour les scripts d\'automatisation
   |_  .gitignore                       // !important pour protéger les secrets AWS
   |_  aws-cred-export.sh *             // à générer par le script generate-aws-cred.sh
   |_  aws-tf.sh                        // Permet d\'allouer et configurer une instance sur AWS pour le déploiement
   |_  generate-aws-cred.sh             // Pour générer les creds d\'AWS (ID, SECRET)
   |_  generate-reg-cred.sh             // Pour générer les secrets d\'accés au registre *GitLab* 
   |_  Readme.md                        // Instructions du projet
```

# Pipelines
La même structure de pipeline est partagée par les deux projets frontend et backend.
## Build Image
Cette étape permet de construire et mettre l'image dans le registre privé du projet.
Elle marque les images par un timestamp, d'où l'unicité du nom.
Enfin, elle passe ce nom comme artéfact à l'étape suivante.

## Update
Le but est de déployer l'application sur l'instance cible.
La première tâche commence par générer un fichier values.yaml qui contient le tag de l'image récupéré de l'étape précédente.
Cette tâche est réalisée par le script deploy.sh
La deuxième tâche est de déployer un helm chart en utilisant ce fichier de valeurs.

Ce job, contrairement au premier, est lancé par le runner installer sur l'instance.

# Automatisation de la création de l'infrastructure
## Infrastructure as Code - Terraform
Le dossier IaC contient les fichiers Terraform de l'infrastructure désirée. Ces fichiers permettent de créer un *VPC*, *Sous-réseau*, *Security Group*, *Keypair*, *Internet Gateway* et l'Instance *EC2*.

## Configuration - Ansible
Le playbook Ansible permet de configurer l'instance allouée.
Les étapes décrites concernent l'installation de dépendances Ubuntu, Docker et sa config, *Microk8s* et sa config, *GitLab*  Runner.
Ansible permet aussi d'installer la base de données sur *Kubernetes*.

# Installation
0. Forker le projet
1. Modifier la clé publique dans le fichier IaC/keypair.tf
2. Changer les variables Reg_token et tags dans le fichier playbook/runner.yml
3. Générer le script contenant login/mdp de AWS en exécutant

```bash
    chmod +x generate-aws-cred.sh
    ./generate-aws-cred.sh
```
4. Lancer cette commande pour allouer et configurer une instance sur AWS
```bash
    chmod +x aws-tf.sh
    ./aws-tf.sh
```
5.  Configurer sur la machine EC2 les credentials et secrets du registre *GitLab*  en utilisant le script
```bash
    chmod +x generate-reg-cred.sh
    ./generate-reg-cred.sh
```
6. Déployer l'application en utilisant le pipeline

7. Ajouter **vapormap.com** à /etc/hosts pour résoudre l'IP du serveur.

8. Naviguer vers **vapormaps.com**