FROM python:alpine3.15
WORKDIR /app
COPY ./src/requirements ./requirements
# RUN apk add libssl-dev
RUN pip3 install --upgrade pip && \
    pip3 install wheel && \
    pip3 install -r requirements/production.txt
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \ 
    VAPOR_DBNAME=db_vapormap \
    VAPOR_DBUSER=user_vapormap \
    VAPOR_DBPASS=vapormap \
    VAPOR_DBHOST=mysql \
    DJANGO_SETTINGS_MODULE="vapormap.settings.production"
COPY ./src/. .
CMD ["gunicorn" ,"vapormap.wsgi:application", "--bind", "0.0.0.0:8001"]