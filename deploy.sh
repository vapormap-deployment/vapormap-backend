#!/bin/bash
cp _chart/values.yaml values.yaml
echo "" >> values.yaml
echo "  image: \"$CI_REGISTRY_IMAGE$TAG_IMG-app\"" >> values.yaml
echo "  migrationimage: \"$CI_REGISTRY_IMAGE$TAG_IMG-mg\"" >> values.yaml
cat values.yaml